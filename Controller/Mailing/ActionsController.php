<?php

namespace Nitra\BuyerReportsBundle\Controller\Mailing;

use Admingenerated\NitraBuyerReportsBundle\BaseMailingController\ActionsController as BaseActionsController;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * ActionsController
 */
class ActionsController extends BaseActionsController
{
    public function attemptObjectSend($pk)
    {
        $dm       = $this->getDocumentManager();
        $session  = $this->getRequest()->getSession();
        $storeId  = $session->get('store_id');
        $store    = $dm->find('NitraStoreBundle:Store', $storeId);
        $mailing  = $this->getObject($pk);

        $mailer   = $this->get('mailer');
        $bTotal   = 0;
        $bSuccess = 0;
        foreach ($mailing->getBuyers() as $buyer) {
            $message = \Swift_Message::newInstance()
                ->setSubject($mailing->getSubject())
                ->setFrom($store->getMailingEmail())
                ->setTo($buyer->getEmail())
                ->setBody($mailing->getText(), 'text/html');
            if ($mailer->send($message)) {
                $bSuccess ++;
            }
            $bTotal ++;
        }


        if (($bSuccess == 0) && ($bSuccess != $bTotal)) {
            $type = 'error';
        } elseif ($bSuccess < $bTotal) {
            $type = 'warning';
        } else {
            $type = 'success';
        }
        $msg      = $this->get('translator')->trans("actions.mailing.send.$type", array(
            '{successs}'    => $bSuccess,
            '{all}'         => $bTotal,
            '{error}'       => $bTotal - $bSuccess,
        ), 'NitraBuyerReportsBundle');

        $this->get('session')->getFlashBag()->add($type, $msg);

        return new RedirectResponse($this->generateUrl("Nitra_BuyerReportsBundle_Mailing_list"));
    }
}