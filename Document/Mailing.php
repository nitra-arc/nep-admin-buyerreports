<?php

namespace Nitra\BuyerReportsBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\Document
 */
class Mailing
{
    /**
     * @ODM\Id(strategy="AUTO")
     */
    private $id;

    /**
     * Название
     * @ODM\String
     * @Gedmo\Translatable
     */
    private $name;

    /**
     * Тема
     * @ODM\String
     * @Gedmo\Translatable
     */
    private $subject;

    /**
     * Название
     * @ODM\String
     * @Gedmo\Translatable
     */
    private $text;

    /**
     * @ODM\ReferenceMany(targetDocument="Nitra\BuyerBundle\Document\Buyer")
     */
    private $buyers;

    public function __construct()
    {
        $this->buyers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return self
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * Get text
     *
     * @return string $text
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Add buyer
     *
     * @param Nitra\BuyerBundle\Document\Buyer $buyer
     */
    public function addBuyer(\Nitra\BuyerBundle\Document\Buyer $buyer)
    {
        $this->buyers[] = $buyer;
    }

    /**
     * Remove buyer
     *
     * @param Nitra\BuyerBundle\Document\Buyer $buyer
     */
    public function removeBuyer(\Nitra\BuyerBundle\Document\Buyer $buyer)
    {
        $this->buyers->removeElement($buyer);
    }

    /**
     * Get buyers
     *
     * @return Doctrine\Common\Collections\Collection $buyers
     */
    public function getBuyers()
    {
        return $this->buyers;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return self
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * Get subject
     *
     * @return string $subject
     */
    public function getSubject()
    {
        return $this->subject;
    }
}